var numSquares = 6;
var colors = generateRndomColors(numSquares);
var squares = document.querySelectorAll(".square");
var colorDisplay = document.getElementById("colorDisplay");
var pickedColor = pickColor();
var h1 = document.querySelector("h1");
var resetButton = document.querySelector("#reset");
var easyBtn = document.querySelector("#easyBtn");
var hardBtn = document.querySelector("#hardBtn");

easyBtn.addEventListener("click", function(){
	easyBtn.classList.add("selected");
	hardBtn.classList.remove("selected");
	numSquares = 3;
	colors = generateRndomColors(numSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor; 
	for(var i = 0; i < squares.length; i++)
	{
		if(colors[i]){
			squares[i].style.background = colors[i];
		} else {
			squares[i].style.display = "none";
		}
	}
});

hardBtn.addEventListener("click", function(){
	hardBtn.classList.add("selected");
	easyBtn.classList.remove("selected");
	numSquares = 6;
	colors = generateRndomColors(numSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor; 
	for(var i = 0; i < squares.length; i++)
	{
			squares[i].style.background = colors[i]; 
			squares[i].style.display = "block";
	}
});

resetButton.addEventListener("click", function(){
	colors = generateRndomColors(numSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	messageDisplay.textContent = " ";
	for(var i = 0; i < squares.length;i++){
	squares[i].style.background = colors[i]
	}
	h1.style.background = "steelblue";
	resetButton.textContent = "New Colors";
});

var messageDisplay = document.querySelector("#message");
colorDisplay.textContent=pickedColor;
for(var i = 0; i < squares.length;i++){
	squares[i].style.background = colors[i];

	squares[i].addEventListener("click",function(){
		var clickedColor = this.style.background;
		if(clickedColor === pickedColor){
			messageDisplay.textContent = "Correct"
			resetButton.textContent = "Play Again ?";
			changeColors(clickedColor);
			h1.style.background = clickedColor;

		} else {
			this.style.background = "black";
			messageDisplay.textContent = "Try Again"
		}
	})
}

function changeColors(color){
	for(var i = 0;i <squares.length;i++){
		squares[i].style.background = color;
	}
}

function pickColor(){
	var random = Math.floor(Math.random() * colors.length);
	return colors[random];
}

function generateRndomColors(num){
	var arr = [];
	for(var i = 0; i < num; i++){
		arr.push(randomColor());
	}

	return arr;
}

function randomColor(){
	var r = Math.floor(Math.random() * 256);
	var g = Math.floor(Math.random() * 256);
	var b = Math.floor(Math.random() * 256);
//rgb(r, g, b);
	return "rgb(" + r + ", " + g + ", " + b + ")";
}

// funtion attempt(){
// 	for( var i = 1;i <= 3;i++){
		
// 	}
// }
